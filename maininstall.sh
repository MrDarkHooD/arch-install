#!/bin/sh

timedatectl set-ntp true

fdisk -l
read -p 'Device1: ' device1
read -p 'Device2: ' device2

mkfs.fat -F32 /dev/$device1
mkfs.ext4 /dev/$device2

mount /dev/$device2 /mnt
mkdir /mnt/boot
mount /dev/$device1 /mnt/boot

pacstrap /mnt base linux linux-firmware sudo grub efibootmgr networkmanager intel-ucode emacs

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
