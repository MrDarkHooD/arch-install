#!/bin/bash
fn=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13).png

case $1 in
	snip)
		scrot -s $fn
		;;

	active)
		scrot -u $fn
		;;

	all)
		scrot $fn
		;;

	*)
		geo=$(
			i3-msg -t get_outputs | \
				jq -r '
					.[] |
					select(
						.name=="'$(x-focused-output)'"
					).rect |
					"\(.x),\(.y),\(.width),\(.height)"
				'
			)

		scrot -a $geo $fn
		;;
esac

mv $fn ~/screenshots/
