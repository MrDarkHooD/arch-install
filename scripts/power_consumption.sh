#!/bin/sh

time=1
declare T0=($(sudo cat /sys/class/powercap/*/energy_uj));
sleep $time;
declare T1=($(sudo cat /sys/class/powercap/*/energy_uj))

total_power=0

for i in "${!T0[@]}"; do
  power=$(awk "BEGIN {print (${T1[i]} - ${T0[i]}) / $time / 1e6}")
  total_power=$(awk "BEGIN {print $total_power + $power}")
done

printf "%.1f W\n" $total_power
