#!/bin/sh

mode=$(
	i3-msg \
		-t get_binding_state | \
	jq \
		-r '
			.name
		'
)

if [ "$mode" == "resize" ]; then
	echo "%{B#A54242} r %{B-}"
else
	echo ""
fi
