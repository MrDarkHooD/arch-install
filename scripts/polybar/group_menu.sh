#!/bin/sh
if [ "$1" == "all" ]; then
	groups=$(
		i3-workspace-groups \
			list-groups | \
		sed 's/ /\n/g'
	)
else
	groups=$(
		i3-workspace-groups \
			list-groups \
			--focused-monitor-only | \
		sed 's/ /\n/g'
	)
fi

declare excluded
excluded=(Porn)

for del in "${excluded[@]}"
do
	groups=("${groups[@]/$del}")
done

choice=$(echo -e "$groups" | sort | \
	rofi \
		-dmenu \
		-p "Select workspace to focus into:"
)

# Execute the script with the selected option
if [ -n "$choice" ]; then
	echo $choice
	i3-workspace-groups switch-active-group $choice
else
	echo "no choise"
fi
