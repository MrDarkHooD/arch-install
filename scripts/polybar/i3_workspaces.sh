#!/bin/sh

current_group=$(i3-groups-current "$1")

ws_in_active_group=$(
i3-groups-json-tree | \
jq \
	--arg curr_group "$current_group" \
	--arg output "$1" \
	-r '
	[ .[] |
	select(
		.group == $curr_group and
		.output == $output
	) ] |
	sort_by(.group_name) | .[] |
	{
		focused: .focused,
		urgent: .urgent,
		name: .group_name
	}
')

echo $ws_in_active_group | jq -c | while read -r json; do
	focused=$(echo $json | jq '.focused')
	urgent=$(echo $json | jq '.urgent')
	name=$(echo $json | jq -r '.name')

	if [ "$focused" = "true" ]; then
		bgcolor=#373B41
	elif [ "$urgent" = "true" ]; then
		bgcolor=#A54242
	else
		bgcolor=#002b36
	fi

	echo -n "%{B$bgcolor}"
	echo -n "%{A1:i3-groups-change-ws $name:} $name %{A}"
	echo -n "%{B-}"

#	echo -n "<default>$name</default>"

done
