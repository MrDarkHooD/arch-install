#!/bin/sh

fdisk -l
read -p 'Device: ' device

ln -sf /usr/share/zoneinfo/Europe/Helsinki /etc/localtime
hwclock --systohc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP="fi"" >> /etc/vconsole.conf

systemctl enable NetworkManager
systemctl enable fstrim.timer

read -p 'Give username:' username

useradd -m -G wheel -s /bin/bash $username

echo "Root password:"
passwd
echo "User password:"
passwd $username

echo $username "ALL=(ALL:ALL) ALL" >> /etc/sudoers

read -p 'Hostname: ' hostname
echo $hostname > /etc/hostname

grub-install /dev/$device --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg
