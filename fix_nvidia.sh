#!/bin/sh

sudo pacman -S nvidia nvidia-utils libvdpau nvidia-libgl nvidia-settings
sudo sed -i 's/kms //' /etc/mkinitcpio.conf
sudo mkinitcpio -P

sudo pacman -S lib32-mesa

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=".*"/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet nvidia_drm.modeset=1"/' /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg

#xorg and i3 related
#sudo pacman -S xorg xorg-xinit xterm dmenu i3-wm i3blocks i3lock picom feh
#yay -S perl-anyevent-i3 #just for in case, enables i3-save-tree
#cp $SCRIPT_DIR/xinitrc ~/.xinitrc
