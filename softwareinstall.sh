#!/bin/sh

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#main stuff
sudo pacman -S --needed base-devel
sudo pacman -S linux-headers
sudo pacman -S git terminator scrot curl jq tree neofetch ffmpeg htop shc

# AUR
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R $USER:$USER ./yay-git
cd yay-git
makepkg -si
sudo yay -Syu

# Pacman
sudo pacman -S pacman-contrib
sudo echo "[multilib]" >> /etc/pacman.conf
sudo echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf

sudo cp $SCRIPT_DIR/config/mirrorlist >> /etc/pacman.d/mirrorlist
sudo -Suy

systemctl enable paccache.timer
systemctl start paccache.timer

#fonts
sudo pacman -S ttf-liberation ttf-unifont noto-fonts noto-fonts-emoji ttf-linux-libertine
yay -S ttf-symbola
mkdir ~/.local/share/fonts
cp $SCRIPT_DIR/resources/AIcons.ttf ~/.local/share/fonts
fc-cache -fv

#ssh
sudo pacman -S openssh
cp $SCRIPT_DIR/ssh/rikka ~/.ssh/
sudo chmod 600 ~/.ssh/rikka

#GPU drivers for NVIDIA GeForce GTX 1070
sudo pacman -S nvidia nvidia-utils libvdpau nvidia-libgl nvidia-settings
sudo sed -i 's/kms //' /etc/mkinitcpio.conf
sudo mkinitcpio -P

sudo pacman -S lib32-mesa

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=".*"/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet nvidia_drm.modeset=1"/' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

#xorg and i3 related
sudo pacman -S xorg xorg-xinit xterm dmenu i3-wm i3blocks i3lock picom feh
yay -S perl-anyevent-i3 #just for in case, enables i3-save-tree
cp $SCRIPT_DIR/xinitrc ~/.xinitrc

sudo cp $SCRIPT_DIR/config/i3blocks.conf /etc/i3blocks.conf
sudo cp $SCRIPT_DIR/config/picom.conf /etc/xdg/picom.conf
sudo cp $SCRIPT_DIR/config/i3 ~/.config/i3/config

sudo pacman -S imagemagick
mkdir ~/git
cd ~/git
git clone https://github.com/meskarune/i3lock-fancy.git
cd i3lock-fancy
sudo make install

# Shell
#sudo pacman -S fish
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
omf install agnoster
omf theme agnoster

# Terminal
sudo pacman -S urxvt rxvt-unicode xse
yay -S ttf-iosevk
sudo cp $SCRIPT_DIR/config/Xresources ~/.Xresources

## File management and drives
# file explorers
sudo pacman -S thunar
yay -S raw-thumbnailer ffmpegthumbnailer

# Drive handlers
sudo pacman -S thunar-volman gvfs udiskie2 polkit
sudo cp $SCRIPT_DIR/config/10-udisks2.rules /etc/polkit-1/rules.d/
sudo cp $SCRIPT_DIR/config/20-shutdown-reboot.rules /etc/polkit-1/rules.d/ #Yea not drive handler but whatever

## Notifications
sudo pacman -S notification-daemon
sudo echo "[D-BUS Service]" > /usr/share/dbus-1/services/org.freedesktop.Notifications.service
sudo echo "Name=org.freedesktop.Notifications" >> /usr/share/dbus-1/services/org.freedesktop.Notifications.service
sudo echo "Exec=/usr/lib/notification-daemon-1.0/notification-daemon" >> /usr/share/dbus-1/services/org.freedesktop.Notifications.service

## zip etc
sudo pacman -S p7zip

#editors
sudo pacman -S emacas notepadqq
sudo cp $SCRIPT_DIR/config/emacs ~/.emacs

#bluetooth
sudo pacman -S bluez bluez-utils bluez-plugins

#sound
sudo pacman -S alsa-utils pulseaudio pulseaudio-alsa pulseaudio-bluetooth pavucontrol

## Multimedia
sudo pacman -S vlc obs-studio

## Benchmark
yay -S mangohud

## Browsers
sudo pacman -S firefox chromium gimp

#This doesn't actually work, reason unknown, probably because when chrome starts first time it sets itself primary browser
unset BROWSER
xdg-settings set default-web-browser firefox.desktop

## Chats
sudo pacman -S telegram-desktop discord

# Performance
sudo pacman -S mangohud

## Games
#Steam
sudo pacman -S steam
#Missing performance mode setup
#(mangohud steam-runtime)

#ps1
yay -S epsxe
usermod -G games -a $USER
sudo ln -s /usr/bin/epsxe /usr/bin/playstation1

#ps2
yay -S pcsx2
sudo ln -s /usr/bin/pcsx2-qt /usr/bin/playstation2

#ps3
yay -S rpcs3-git
sudo ln -s /usr/bin/rpcs3 /usr/bin/playstation3

#Xbox360
#yay -S xenia-git
#sudo ln -s /usr/bin/xenia /usr/bin/xbox360

## Virtual machines
# qemu
sudo pacman -S qemu qemu-arch-extra

# VirtualBox
sudo pacman -S virtualbox virtualbox-host-modules-arch
sudo modprobe vboxdrv

## Coding

# Python
sudo pacman -S python python3-pip

# OSdev
yay -S i686-elf-gcc
sudo pacman -S mtools libisoburn

# Own scripts
mkdir ~/.scripts
mkdir ~/screenshots
mkdir ~/.wallpapers
cp $SCRIPT_DIR/scripts ~/.scripts
cp wallpapers/ ~/.wallpapers/

# Set default programs
xdg-mime default mpv.desktop application/octet-stream
xdg-mime default mpv.desktop video/mp4
xdg-settings set default-web-browser firefox.desktop
xdg-mime default mpv.desktop video/webm
xdg-mime default feh.desktop image/jpeg
xdg-mime default feh.desktop image/png
xdg-settings set default-url-scheme-handler firefox.desktop
